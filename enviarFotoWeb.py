#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Este script se ejecuta una vez al día mediante cron, y envia una foto mediante el metod POST a un servidor web
que la recibe, de forma que la web siempre muestra lá foto actualizada del día.
"""
import time
import requests

# Prepara el nombre de la foto
nombrefoto=time.strftime("%Y%m%d_1201")

# Dirección de la web, debe tener el archivo p_sub.php, que es el encargado de recibir la imagen.
url='https://direccion/pagina/web/p_sub.php?'

# Nosotros usamos una version escalada que hemos generado con el archvo 'capturarWeb.sh', pero si quieres ...
foto={'photo':open('/ruta/carpeta/fotos/ultimaWeb.jpg', 'rb')}

# ... puedes usar una foto sin modificar, descomentando la línea de abajo y comentando la de arriba.
#foto={'photo':open('/ruta/carpeta/fotos/'+nombrefoto+'.jpg', 'rb')}

# Envia la foto, la url del ejemplo es https, con la opción verify=True comporbamos el certificado https, si quieres ignorarlo
# (algo que no te aconsejamos, salvo que estes muy seguro) cambia esta opción a verify=False
requests.post(url, files=foto, verify=True)

# Estadísticas
log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+",1\n"
f = open ('/ruta/carpeta/de/estadisticas/fotos_subidas_web.log','a')
f.write(log)
f.close()
