/*
Este script (muy básico) es el encargado de recibir la foto que el bot envia al servidor web y guardarlo en la carpeta
especificada. Necesitaras un servidor con php para poder ejecutarlo.

Este script solo admite archivos de tipo jpg y con un tamaño igual o menor a 1 Mb (1024 Kb), ten esto en cuenta
si modificas el archivo 'enviarFotoWeb.py' del bot y envias fotos más grandes. Ten en cuenta que la instalción por defecto
de un servidor php limita el tamaño máximo de archivos a 2 Mb. y aunque amplies aquí el límite si no tienes permiso para
enviar archivos más grandes este script no funcionará.

Este script guarda la foto en la misma carpeta donde se ejecuta, asegurate de tener permisos de escritura para la
carpeta donde quieras guardar la imagen recibida.

Antes de usar este script te aconsejamos que lo securices un poco más. Este script solo lo preparamos para mostrar alguna
funcionalidad más que se le puede dar al bot, y que para la demostración ya nos sirvio pero no lo tenemos en uso real.
*/
<?php

if (is_uploaded_file($_FILES['photo']['tmp_name'])) {

	$archivoRutaTemporal = $_FILES['photo']['tmp_name'];
	$tamanoArchivo = $_FILES['photo']['size'];
	$tipoArchivo = $_FILES['photo']['type'];
	$nombreArchivo = $_FILES['photo']['name'];
	$nombreArchivoCmps = explode(".", $nombreArchivo);
	$extensionArchivo = strtolower(end($nombreArchivoCmps));

	if ($extensionArchivo == "jpg") {
		if ($tamanoArchivo < 1024000) {
			move_uploaded_file($archivoRutaTemporal, "ultimaFoto.jpg");
		}

	}
}
?>
