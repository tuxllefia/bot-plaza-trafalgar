#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Este script es el encargado de registrar la temperatura y humedad relativa, se ejecuta mediante un cron cada 5 minutos.

Se ejecuta en segundo plano y no hya ningún tipo de interacción con el usuario.

Genera un archivo de tipo CSV con la fecha, hora y las dos lecturas. Necesita la libreria externa Adafruit_DHT que puedes
encontrar aquí https://github.com/adafruit/Adafruit_Python_DHT
"""

import time
import sys
import Adafruit_DHT

humedad, temperatura = Adafruit_DHT.read_retry(22, 5)
log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+","+str(round(temperatura,2))+","+str(round(humedad,2))+"\n"
f = open ('/ruta/carpeta/de/estadisticas/temperaturas_registradas_'+time.strftime("%m")+'.log','a')
f.write(log)
f.close()
