## Bot de la Plaza Trafalgar

### Descripción

En este repositorio encontraras los archivos que hemos usado para crear nuestro sencillo Bot. Con el podemos seguir en directo las obras que se están realizando (o se realizaron depende de cuando llegues aquí) durante el este año 2.019 en la Plaza Trafalgar de Badalona. Y disponemos de las siguientes funciones:
1. Captura automática de fotografías.
2. Envío de imágenes bajo demanda.
3. Suscripción/cancelación para el envío de imágenes automática.
4. Envío del link a la web del proyecto.
5. Envío automático de imágenes a un servidor web.
6. Seguimiento de las diferentes fases de la obra.
7. Captura automática de las condiciones ambientales (humedad y temperatura) de la plaza.
8. Envío a bajo demanda de las condiciones ambientales de la plaza.

El bot funciona sobre una Raspberry Pi 3 B+ con una raspicam y Raspbian (sin entorno gráfico) como sistema operativo. Todo colocado dentro de una caja eléctrica apta para exteriores. Con conexión a Internet para poder interactuar con él, pero con acceso únicamente desde nuestra red local.
Hemos comentado todos los scripts para hacerlos más entendibles.


### Funcionamiento

Todo el bot esta básicamente controlado con un cron. Al iniciarse el bot una linea @reboot en el cron lanza el proceso principal del bot *botPlaza.py*, que es con el que podemos interactuar. Con @reboot aseguramos que aunque tengamos un corte en el suministro eléctrico el proceso se inicie automáticamente con el re-establecimiento del mismo sin necesidad de nuestra intervención.
Además de este proceso principal tenemos 5 más que se mantienen en segundo plano y son complementarios a este, pero con los que no podemos interactuar.

El primero es *capturar.sh*, que se ejecuta cada 15 minutos y toma una foto de la plaza. Estas fotos se utilizarán en el futuro para generar un vídeo de timelapse, y son las fotos que se envían a petición del usuario.

El segundo proceso complementario es *capturarWeb.sh*, usa una foto tomada por el proceso anterior y la prepara para ser enviada a una página web.

El proceso *enviarFotoDiaria.py* envía las fotos a los suscriptores una vez al día, a diferencia del proceso principal estas se envían mediante el método POST usando una URL.

El proceso *enviarFotoWeb.py* envia la foto preparada por *capturarWeb.sh* a una URL especificada mediante el método POST a través de la URL.

Por último el proceso *registrarTemperaturas.py* se ejecuta cada 5 minutos y mantiene un registro de la temperatura y humedad relativa de la plaza mientras el bot esta activo.

Tenemos un último archivo que no forma parte del bot, pero que complementa a *enviarFotoWeb.py* y es *p_sub.php*, este archivo colocado en el servidor de destino es el encargado de recibir la foto enviada por el bot.


### Librerías externas

Este bot funciona básicamente con Python, pero hemos usado librerías externas que nos han ahorrado muchísimo trabajo. En el proceso principal *botPlaza.py* hemos necesitado el uso de **PYTHON-TELEGRAM-BOT** que puedes encontrar en la siguiente dirección https://github.com/python-telegram-bot/python-telegram-bot, también en este proceso hemos necesitado la librería Adafruit_Python_DHT para el uso del sensor de humedad y temperatura, la puedes encontrar aquí https://github.com/adafruit/Adafruit_Python_DHT. Esta última librería también se necesita en *registrarTemperaturas.py*. Sin embargo, si no vas usar sensor de temperatura y humedad no la necesitas, con lo que puedes ignorarla, eso sí, asegurate de quitarla de los diferentes scripts.
