#!/bin/bash
# Este script prepara una copia de una foto concreta (en este caso la de las 12:01 de cada día)
# para poder enviarla a una web y de que en la portada de esta web siempre este la foto actualizada
# del día.
# Este script también se gestiona mediante un cron.

# Prepara el nombre del archivo para la foto que debe usar.
DATE=$(date +"%Y%m%d_1201")

# Escala la foto a un tamaño más pequeño para adaptarlo a la web y la guarda
convert /ruta/carpeta/de/fotos/$DATE.jpg -scale 25% /ruta/carpeta/de/fotos/ultimaWeb.jpg
