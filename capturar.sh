#!/bin/bash
# Este script de bash es el encargado de hacer la captura de la foto y guardarla en la carpeta correspondiente
# se ejecuta mediante un cron cada 15 minutos.
# Para facilitar el acceso a las imagenes las guarda usando para ello la fecha y hora de la captura como nombre
# de archivo, y para mantenerlas ordenadas correlativamente y que no se mezclen las de los diferentes meses
# coloca la fecha 'al reves', AñoMesDia_HoraMinuto.jpg

# Prepara el nombre del archivo
DATE=$(date +"%Y%m%d_%H%M")

# Captura la imagen
raspistill -vf -hf -o /ruta/carpeta/de/fotos/$DATE.jpg

# La recorta para eliminar la parte superior, no queremos que salga, en las imagenes.
convert /ruta/carpeta/de/fotos/$DATE.jpg -crop 3280x1973+0+491 /ruta/carpeta/de/fotos/$DATE.jpg
