#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Este script usa una librería externa, PYTHON-TELEGRAM-BOT, que puesde encontrar en https://github.com/python-telegram-bot/python-telegram-bot.

import logging
import time
import sys
import Adafruit_DHT
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from datetime import date

# Activamos la función de registro de eventos
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)


# Definimos las diferentes funciones para los comandos
def start(bot, update):
    """Envia el mensaje de bienvenida cuando el usuario inicia el bot con el comando /start."""
    bot.send_message(chat_id=update.message.chat_id, text='Hola!!! Bienvenido al bot de la plaza Trafalgar.\n\n   - Conmigo puedes comprobar el estado de las obras (casi) en directo. Hago una foto cada quince minutos, estaré encantado de enviarte la última foto realizada cuando lo solicites.\n\n   - También puedo enviarte una foto cada día de forma automática si quieres.\n\n   - Y si necesitas más información te puedo dar el enlace a una web con planos, noticias, etc...\n\n- Dispongo de un comando que te permite saber que temperatura hace en la plaza, y esta si es en directo.')

def ayuda(bot, update):
    """
    Envia un mensaje de ayuda con todos los comandos cuando el usuario usa el comando /ayuda.
    Si no usas sensor de temperatura y humedad asegurate de borrarlo de la lista de comandos.
    """
    bot.send_message(chat_id=update.message.chat_id, text='/ultimafoto - Te envio la última foto tomada de la plaza.\n/cadadia - Cada día te enviare una foto.\n/cancelardiario - Anulo tu suscripción, dejarás de recibir fotos cada día.\n/temperatura - Te envio la temperatura y humedad relativa actual en la plaza Trafalgar.\n/infoweb - Información: planos, noticias...\n/estado - Muestra las diferentes fases del proyecto.\n/ayuda - Muestra esta ayuda.')

def infoweb(bot,update):
	"""Envia un enlace a la web del proyecto cuando el usuario lanza el comando /infoweb."""
    bot.send_message(chat_id=update.message.chat_id, text='Información: planos, noticias...\n\nhttp://trafalgar.llefia.org/')
    """
    El bot guarda estadísticas ciegas de uso, nosotros solo guaramos la fecha y hora de la petición y lo guardamos en un archivo
    de tipo CSV. Lo hacemos así por que solo nos interesa el numero de peticiones, nada más. Asegurate de cambiar la ruta para
    que apunte a una carpeta y archivo real de tu instalación.
    """
    """Si quieres que el bot registre más datos deberas modificar estas líneas."""
    log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+",1\n"
    f = open ('/ruta/carpeta/de/estadisticas/web_solicitadas.log','a')
    f.write(log)
    f.close()

def temperatura(bot,update):
	"""
	Envia un mensaje con la temperatura y humedad relativa cuando el usuario lanza el comando /temperatura.
	Para esta función necesitas tener instalado un sensor de temeperatura y humedad del tipo DHT 11 o DHT 12
	y la libreria externa Adafruit_DHT (https://github.com/adafruit/Adafruit_Python_DHT). Si no dispones de 
	sensor elimina esta fución o comentala para	que no se tenga en cuenta durante la ejecución, haz lo mismo
	con la linea del manipulador de comandos al final del archivo. Tampoco lo pongas como comando disponible
	en la ayuda.
	"""
    humedad, temperatura = Adafruit_DHT.read_retry(22, 5)
    bot.send_message(chat_id=update.message.chat_id, text='Ahora la temperatura en la Plaza Trafalgar de Badalona es de '+str(round(temperatura,2))+'ºC y una humedad relativa del '+str(round(humedad,2))+'%')
    """Estadísticas de peticiones de temperatura."""
    log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+",1,"+str(round(temperatura,2))+","+str(round(humedad,2))+"\n"
    f = open ('/ruta/carpeta/de/estadisticas/temperatura_solicitdas.log','a')
    f.write(log)
    f.close()

def ultimafoto(bot, update):
	"""
	Envia la una foto junto con un mensaje indicando la hora de la captura. S diferencia de la temperatura que
	hace la captura y la envia en directo esta función usa la últma foto capturada. En nuestro caso la foto se toma
	utilizando un cron 4 veces cada hora, en los minutos 1, 16, 31 y 46.
	"""
	"""Si quieres que el bot envie las fotos en directo deberas modificar esta función."""
    minuto=int(time.strftime("%M"))
    fecha=time.strftime("%Y%m%d_%H")
    if minuto < 16 and minuto > 0:
        fecha += "01"
        minuto="01"
    elif minuto < 31 and minuto > 15:
        fecha += "16"
        minuto="16"
    elif minuto < 46 and minuto > 30:
        fecha += "31"
        minuto="31"
    else:
        fecha += "46"
        minuto="46"

    fecha_foto=time.strftime("%d/%m/%Y, %H:")+minuto
    bot.send_photo(chat_id=update.message.chat_id, photo=open('/ruta/carpeta/fotos/'+fecha+'.jpg', 'rb'))
    bot.send_message(chat_id=update.message.chat_id, text="Fotografia tomada el "+fecha_foto)

    """De nuevo, registro estadístico."""
    log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+",1\n"
    f = open ('/ruta/carpeta/de/estadisticas/fotosenviadas.log','a')
    f.write(log)
    f.close()

def cadadia (bot, update):
	"""
	Esta función suscribe al usuario para que el bot le envie una foto de forma automática cada día cuando se usa el 
	comando /cadadia.
	"""
    
    """
    El bot captura el número de identificación único del usuario en telegram y lo guarga en un archivo, sin ninguna otra
    información. Antes de guardarlo evalua que no exista ya en la suscripción para evitar enviar más de 1 foto.
    """
    id_usuario=str(update.message.chat_id)

    archivo_origen = open ('/ruta/al/archivo/de/suscripciones/cadadia.txt','r')
    lineas = archivo_origen.readlines()
    archivo_origen.close()

    existe=0

    for linea in lineas:
        if linea == id_usuario+'\n':
            existe=1
    
    if existe==0:
        f = open ('/ruta/al/archivo/de/suscripciones/cadadia.txt','a')
        f.write(id_usuario+"\n")
        f.close()
        bot.send_message(chat_id=update.message.chat_id, text="¡¡¡Anotado!!! Te enviare una foto cada día. Si quieres dejar de recibir las fotos pulsa aqui: /cancelardiario")
        log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+",1\n"
        """Más estadísticas."""
        f = open ('/ruta/carpeta/de/estadisticas/estadisticas/suscripciones.log','a')
        f.write(log)
        f.close()
    elif existe==1:
        bot.send_message(chat_id=update.message.chat_id, text="Ya existes en la base de datos.")

def cancelardiario (bot, update):
	"""
	Lanzando el comando /cancelardiario se anula la suscripción a la foto diaria. Igual que en el comando de alta se evalua que
	el usuario a eliminar exista en el archivo antes de intentar eliminarlo.
	"""
    encontrado=0
    id_usuario=str(update.message.chat_id)
    archivo_origen = open ('/ruta/al/archivo/de/suscripciones/cadadia.txt','r')
    lineas = archivo_origen.readlines()
    archivo_origen.close()

    archivo_destino = open ('/ruta/al/archivo/de/suscripciones/cadadia.txt','w')
    for linea in lineas:
        if linea != id_usuario + '\n':
            archivo_destino.write(linea)
        else:
            encontrado=1
            log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+",-1\n"
            """Estadísitcas"""
            f = open ('/ruta/carpeta/de/estadisticas/suscripciones.log','a')
            f.write(log)
            f.close()
    archivo_destino.close()

    bot.send_message(chat_id=update.message.chat_id, text="Hecho, ya te hemos eliminado de nuestra base de datos, ya no te llegarán más fotos. Si quieres volver a recibirlas pulsa aqui: /cadadia")

def estado (bot, update):
	"""
	Esta función te indica el estado de las obras en base a la fecha de la petición. Nosotros teniamos acceso al plan de obra
	con lo que pudimos trasladarlo al bot. Como puedes ver esta función no es más que un montón de evaluaciones condicionales.
	Si la fecha de la petición esta dentro del rango de esa condicional muestra el mensaje de esa fase. Se puede dar el caso que
	el mensaje con la diferentes fases sea muy largo , con lo que para hacerlo más legible también le añadimos un contador 
	que coloca el número de la fase.
	También indica cuantos días quedan para la finalización de la obra.
	"""
    ano=int(time.strftime("%Y"))
    mensual=int(time.strftime("%m"))
    dia=int(time.strftime("%d"))

    mensaje="SEGUIMIENTO DE LAS DIFERENTES FASES DE LA OBRA\n\n"
    contador=1

    if ano!=2019:
        bot.send_message(chat_id=update.message.chat_id, text="Las obras de la plaza Trafalgar se llevan a cabo dentro del año 2.019")
    else:
        if mensual>=4 or mensual<=10:
            if mensual ==4 :
                mensaje+=str(contador)+"- Aún estamos en la fase de derribos y movimientos de tierras. Esta fase se alargará hasta el día 20 de mayo.\n\n"
                contador+=1
            if mensual==5 and dia<20:
                mensaje+=str(contador)+"- Aún estamos en la fase de derribos y movimientos de tierras. Esta fase se alargará hasta el día 20 de mayo.\n\n"
                contador+=1
            if mensual==5 and dia==20:
                mensaje+=str(contador)+"- Hoy finaliza la fase de derribos y movimientos de tierras.\n\n"
                contador+=1
            if mensual==4 and dia==30:
                mensaje+=str(contador)+"- Hoy empiezan las obras de la red de saneamiento. Esta fase se alargará hasta del 20 de junio.\n\n"
                contador+=1
            if mensual==5:
                mensaje+=str(contador)+"- Aún estamos en la obras pertenecientes a la red de saneamiento. Esta fase se alargará hasta del 20 de junio.\n\n"
                contador+=1
            if mensual==6 and dia<20:
                mensaje+=str(contador)+"- Aún estamos en la obras pertenecientes a la red de saneamiento. Esta fase se alargará hasta del 20 de junio.\n\n"
                contador+=1
            if mensual==6 and dia==20:
                mensaje+=str(contador)+"- Hoy finalizan las obras de la red de saneamiento.\n\n"
                contador+=1
            if mensual==5 and dia==9:
                mensaje+=str(contador)+"- Hoy empiezan las obras que afectan a las estructuras, muros, las redes de riego e iluminación. Esta fase se alargará hasta el 5 de junio.\n\n"
                contador+=1
            if mensual==5 and dia>9:
                mensaje+=str(contador)+"- Aún estamos en la fase de construcción de estructuras, muros, las redes de riego e iluminación. Esta fase se alargará hasta el 5 de junio.\n\n"
                contador+=1
            if mensual==6 and dia<5:
                mensaje+=str(contador)+"- Aún estamos en la fase de construcción de estructuras, muros, las redes de riego e iluminación. Esta fase se alargará hasta el 5 de junio.\n\n"
                contador+=1
            if mensual==6 and dia==5:
                mensaje+=str(contador)+"- Hoy finaliza la fase de construcción de estructuras, muros, las redes de riego e iluminación\n\n"
                contador+=1
            if mensual==5 and dia==27:
                mensaje+=str(contador)+"- Hoy empieza la fase de construcción de suelos y asfaltado. Esta fase se inicia con la colocación de las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==5 and dia>27:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con la colocación de las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==6 and dia<=3:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con la colocación de las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==6 and dia<14 and dia>=3:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con la colocación de las aceras y con el asfaltado de la plaza. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==6 and dia>=3 and dia>=14 and dia<24:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con el asfaltado la plaza y las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==6 and dia>=3 and dia>=14 and dia>=24:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con el asfaltado la plaza, con la grada y las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==7 and dia<19:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con la grada y las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==7 and dia>=19:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==8 and dia<2:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de suelos y asfaltado. Hoy siguen con las aceras. Esta fase se alargara hasta el 2 de agosto.\n\n"
                contador+=1
            if mensual==7 and dia>=29:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de colocación de mobiliario urbano y señalización. Esta fase se alargara hasta el 20 de septiembre.\n\n"
                contador+=1
            if mensual==8:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de colocación de mobiliario urbano y señalización. Esta fase se alargara hasta el 20 de septiembre.\n\n"
                contador+=1
            if mensual==9 and dia<=20:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de colocación de mobiliario urbano y señalización. Esta fase se alargara hasta el 20 de septiembre.\n\n"
                contador+=1
            if mensual==8 and dia>=14 and dia<=20:
                mensaje+=str(contador)+"- Estamos en la fase de construcción de las pistas de petanca. Esta fase se alargara hasta el 20 de agosto.\n\n"
                contador+=1
            if mensual==8 and dia>=19:
                mensaje+=str(contador)+"- Estamos en las fases finales de ajardinado y ultimas tareas. Esta fase se alargara hasta el 15 de octubre.\n\n"
                contador+=1
            if mensual==9:
                mensaje+=str(contador)+"- Estamos en las fases finales de ajardinado y ultimas tareas. Esta fase se alargara hasta el 15 de octubre.\n\n"
                contador+=1
            if mensual==10 and dia<=3:
                mensaje+=str(contador)+"- Estamos en la fase de ajardinado. Esta fase se alargara hasta el 3 de octubre.\n\n"
                contador+=1
            if mensual==6 and dia>=12:
                mensaje+=str(contador)+"- Estamos en la fase para permitir el transito de peatones. Esta fase se alargara hasta el 19 de julio.\n\n"
                contador+=1
            if mensual==7 and dia<=19:
                mensaje+=str(contador)+"- Estamos en la fase para permitir el transito de peatones. Esta fase se alargara hasta el 19 de julio.\n\n"
                contador+=1
            if mensual==7 and dia>=26:
                mensaje+=str(contador)+"- Estamos en la fase para la construcción del edificio de la petanca. Esta fase se alargara hasta el 15 de agosto.\n\n"
                contador+=1
            if mensual==8 and dia<=15:
                mensaje+=str(contador)+"- Estamos en la fase para la construcción del edificio de la petanca. Esta fase se alargara hasta el 15 de agosto.\n\n"
                contador+=1
            if mensual==8 and dia==27:
                mensaje+=str(contador)+"- Hoy se vuelven a abrir las pistas de petanca y se finaliza la fase de acabados en el edificio del CAP y la colocación de la escalera metálica.\n\n"
                contador+=1
            if mensual==8 and dia==5:
                mensaje+=str(contador)+"- Hoy se inicia la fase de acabados en el edificio del CAP y la colocación de la escalera metálica. Esta fase durará hasta el 27 de agosto.\n\n"
                contador+=1                
            if mensual==8 and dia>5 and dia<27:
                mensaje+=str(contador)+"- Estamos en la fase de acabados en el edificio del CAP y colocación de la escalera metálica. Esta fase finaliza el 27 de agosto.\n\n"
                contador+=1
            if mensual==10 and dia>15:
                mensaje+=str(contador)+"- Hoy finalizan las obras de la Plaza Trafalgar.\n\n"
                contador+=1

        fecha_fin=date(2019,10,15)
        fecha_hoy=date(ano,mensual,dia)
        dias=fecha_fin-fecha_hoy
        mensaje+="\nLa fecha prevista para la finalización de las obras es el día 15 de octubre de 2.019, quedan: "+str(dias.days)+" días.\n\nNota: Todas las fechas y estados de las obras proceden del plan de obra."
        bot.send_message(chat_id=update.message.chat_id, text=mensaje)

def error(bot, update):
    """Resgistro de errores ocasionados por los updater del bot."""
    logger.warning('Update "%s" caused error "%s"', bot, update.error)


def main():
    """Inicia el bot."""
    # Genera el updater y le pasa el token del bot.
    # Asegurate de usar use_context=True para usar el nuevo contexto de devoluciones.
    # Para las versiones posteriores a la 12 ya no será necesario.
    # Asegurate de colocar aquí el token de tu bot.
    updater = Updater("Aqui:PonTuToken")

    # Obtiene el despachador para registrar las funciones de los ...
    dp = updater.dispatcher

    # ... diferentes comandos. - Como respuesta en Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("ayuda", ayuda))
    dp.add_handler(CommandHandler("ultimafoto", ultimafoto))
    dp.add_handler(CommandHandler("cadadia", cadadia))
    dp.add_handler(CommandHandler("cancelardiario", cancelardiario))
    dp.add_handler(CommandHandler("temperatura", temperatura)) # borra o comenta esta línea si no tienes sensor de temperatura
    dp.add_handler(CommandHandler("infoweb",infoweb))
    dp.add_handler(CommandHandler("estado",estado))

    # registra todos los errores
    dp.add_error_handler(error)

    # Inicia el bot
    updater.start_polling()

    # Mantiene el bot funcionando mientras no se pulse Ctrl+C, o se pare manualmente.
    updater.idle()


if __name__ == '__main__':
    # Función de entrada
    main()