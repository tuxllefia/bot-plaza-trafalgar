#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Este script se encarga de enviar la foto diaria a los suscriptores. A diferencia del bot que usa la librería 
PYTHON-TELEGRAM-BOT este script envia directamente las fotos y el mensaje usando el metodo POST mediante una url
debidamente formateada. Para modificar cualquier cosa de este archivo te recomendamos que te informes sobre
el uso de Telegram mediante URL y como formatear URL's con request.

Este script se ejecuta una vez al día, mediante cron (en concreto a las 10:05) y envia la foto de las 10:01 junto con
un mensaje. Recorre el archivo de suscriptores y envia una foto a cada uno.

Asegurate de colocar tu token. 
"""

import time
import requests

nombrefoto=time.strftime("%Y%m%d_1001")
contador=0
token='Aqui:VaTuToken'

with open("/ruta/al/archivo/de/suscripciones/cadadia.txt") as lineas:
    for id_usuario in lineas:
        url='https://api.telegram.org/bot'+token+'/sendPhoto?chat_id=' + id_usuario + '&caption=Esta%20es%20la%20foto%20de%20hoy.'
        foto={'photo': open('/ruta/carpeta/de/fotos/'+nombrefoto+'.jpg', 'rb')}
        requests.post(url, files=foto)
        contador+=1

# Estadísitcas (funciona igual que en el bot)
log=time.strftime("%d/%m/%Y")+","+time.strftime("%H:%M")+","+str(contador)+"\n"
f = open ('/ruta/carpeta/de/estadisticas/suscripciones_enviadas.log','a')
f.write(log)
f.close()
